DROP TABLE IF EXISTS nilaiukk;
CREATE TABLE nilaiukk(
    nmUrut int(3) unsigned auto_increment,
    kelas varchar(20) null,
    nmAbsen int(2) null,
    namaSiswa varchar(40) null,
    sesiMulai timestamp,
    sesiAkhir timestamp,
    nmLab int(1) null,
    nilaiWP int(2) default 0,
    nilaiSS int(2) default 0,
    primary key(nmUrut)
);