<?php

class Login extends Controller
{
    public function index(){
        $data['title']='Login Ukks';
        $this->view('template/header',$data);
        $this->view('login/index');
        $this->view('template/footer');
    }

    public function auth(){
        $data['title']='Login Ukks';
        $this->view('template/header',$data);
        $this->view('login/auth');
        $this->view('template/footer');
    }
}