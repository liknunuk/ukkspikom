<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  public function __construct(){
    if(!isset($_SESSION['loggeduser'])){
      header("Location:".BASEURL."Login");
    }
  }

  // method default
  public function index()
  {
    $data['title'] = 'UKKS LKP PIKOM';
    $data['sesi'] = $this->model('Model_ukks')->sesiukk();

    $this->view('template/header',$data);
    $this->view('home/index',$data);
    $this->view('template/footer');
  }

  public function peserta($sesi){
    $data['peserta'] = $this->model('Model_ukks')->peserta($sesi);
    $data['title'] = 'Peserta UKKS '.$sesi;
    $this->view('template/header',$data);
    $this->view('home/peserta',$data);
    $this->view('template/footer');
  }

  public function nilai($nmUrut){
    $data['siswa'] = $this->model('Model_ukks')->siswa($nmUrut);
    $data['title'] = 'Nilai UKKS '.$nmUrut;
    $data['nmUrut'] = $nmUrut;
    $this->view('template/header',$data);
    $this->view('home/siswa',$data);
    $this->view('template/footer');
  }

  public function setNilai(){
    // print_r($_POST);
    if( $this->model('Model_ukks')->setNilai($_POST) > 0 ){
      header("Location:".BASEURL."Home/peserta/".$_POST['nmSesi']);
    }
  }

  public function results($page = 1){
    $data['title'] = "Hasil Perolehan";
    $data['page'] = $page;
    $this->view('template/header',$data);
    $data['nilai'] = $this->model('Model_ukks')->perolehan($page);
    $this->view('home/hasil',$data);
    $this->view('template/footer');
  }
}
