<?php
class Model_ukks
{
    private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    public function sesiukk(){
        
        $sql = "SELECT nmSesi , kelas,nmLab,DATE_FORMAT(sesiMulai,'%d/%m/%Y %H:%i:%s') mulai, DATE_FORMAT(sesiAkhir,'%d/%m/%Y %H:%i:%s') selesai from nilaiukk GROUP BY nmSesi ORDER BY nmSesi,nmLab";
        $this->db->query($sql);

        //eksekusi query setelah binding data
        $this->db->execute();

        // kembalikan hasil pembacaan data, 1 baris;
        return $this->db->resultSet(); 
    }

    public function peserta($sesi){
        $sql = "SELECT nmUrut , nmAbsen , namaSiswa, nilaiWP, nilaiSS FROM nilaiukk WHERE nmSesi = :sesi ";
        $this->db->query($sql);
        $this->db->bind('sesi',$sesi);
        $this->db->execute();
        return $this->db->resultset();
    }

    public function siswa($nmUrut){
        $sql = "SELECT nmUrut , kelas , nmAbsen , namaSiswa, nmSesi FROM nilaiukk WHERE nmUrut = :nmUrut ";
        $this->db->query($sql);
        $this->db->bind('nmUrut',$nmUrut);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function setNilai($data){
        $sql = "UPDATE nilaiukk SET nilaiWP = :nwp , nilaiSS = :nss WHERE nmUrut = :nmUrut";

        $this->db->query($sql);
        $this->db->bind('nwp',$data['nwp']);
        $this->db->bind('nss',$data['nss']);
        $this->db->bind('nmUrut',$data['nmUrut']);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function perolehan($hal){
        $row = 40;
        $pn = ( $hal - 1 ) * $row;
        $sql = "SELECT namaSiswa,kelas,nmAbsen,nilaiWP,nilaiSS FROM nilaiukk ORDER BY kelas, nmAbsen LIMIT {$pn} , {$row}";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

}
