<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="mt-5">
                <form action="<?=BASEURL;?>Login/auth" method="post">
                    <div class="form-group">
                        <input type="text" name="userid" id="userid" class="form-control" placeholder = "Username">
                    </div>
                    <div class="form-group">
                        <input type="password" name="userkey" id="userkey" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<?php $this->view('template/bs4js'); ?>