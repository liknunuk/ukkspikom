
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 bg-dark text-light text-center pg-head">
      <h4>UJI KOMPETENSI KOMPUTER SEKOLAH</h4>
      <h3>LKP PIKOM BANJARNEGARA</h3>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-md-12 text-center">
      <h4>PEROLEHAN NILAI</h4>
    </div>
  </div>    
  <div class="row">
    <div class="col-sm-12">
        <table class="table">
            <thead>
                <tr class='bg-dark text-light'>
                    <th>Nama Peserta</th>
                    <th>Nilai WP</th>
                    <th>Nilai SS</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($data['nilai'] AS $nilai): ?>
                    <tr>
                        <td><?=$nilai['namaSiswa'];?><br><?=$nilai['kelas'].' : '.$nilai['nmAbsen'];?></td>
                        <td class='text-right'><?=$nilai['nilaiWP'];?></td>
                        <td class='text-right'><?=$nilai['nilaiSS'];?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <table class="table">
            <tbody>
                <td class='text-left'>
                    <a class="btn btn-info" id="ppg">Sebelum</a></td>
                <td class='text-center'>
                    <a href="<?=BASEURL;?>">Kembali</a>
                </td>
                <td class='text-right'>
                    <a class="btn btn-info" id="npg">Berikut</a></td>
                </td>
            </tbody>
        </table>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    let pn = <?=$data['page'];?>;
    let hp = "<?=BASEURL;?>";

    $(document).ready( function(){
        $("#ppg").click(function(){
            if( pn == 1 ){
                location.href=hp+`Home/results/1`;
            }else{
                let pp = pn - 1;
                location.href=hp+`Home/results/${pp}`;
            }
        })

        $("#npg").click( function(){
            let np = pn + 1;
            location.href=hp+`Home/results/${np}`;
        })
    })
</script>
