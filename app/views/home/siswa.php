<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 bg-dark text-light text-center pg-head">
      <h4>UJI KOMPETENSI KOMPUTER SEKOLAH</h4>
      <h3>LKP PIKOM BANJARNEGARA</h3>
    </div>
  </div>

  <div class="row mt-3">
    <div class="col-md-12 text-center">
      <h4>NILAI PEROLEHAN</h4>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-sm">
        <tr>
          <td>No. Index</td><td><?=$data['siswa']['nmUrut'];?></td>
        </tr>
        <tr>
          <td>Kelas</td><td><?=$data['siswa']['kelas'];?></td>
        </tr>
        <tr>
          <td>No. Absen</td><td><?=$data['siswa']['nmAbsen'];?></td>
        </tr>
        <tr>
          <td>Nama Siswa</td><td><?=$data['siswa']['namaSiswa'];?></td>
        </tr>
      </table>
      <form action="<?=BASEURL;?>Home/setNilai" method="post">
        
        <input type="hidden" name="nmUrut" value="<?=$data['siswa']['nmUrut'];?>">
        <input type="hidden" name="nmSesi" value="<?=$data['siswa']['nmSesi'];?>">
        
        <div class="form-group">
          <input type="number" name="nwp" id="nwp" class="form-control" placeHolder="Nilai Ms Word" min=50 max=100>
        </div>

        <div class="form-group">
          <input type="number" name="nss" id="nss" class="form-control" placeHolder="Nilai Ms Excel" min=50 max=100>
        </div>

        <div class="form-group text-center">
          <button type="submit" class="btn btn-success">Set Nilai</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
