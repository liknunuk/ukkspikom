<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 bg-dark text-light text-center pg-head">
      <h4>UJI KOMPETENSI KOMPUTER SEKOLAH</h4>
      <h3>LKP PIKOM BANJARNEGARA</h3>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-md-12 text-center">
      <h4>SESSION UJIAN</h4>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
        <div class="list-group">
            <?php foreach($data['sesi'] AS $sesi): ?>
            <li class="list-group-item sesi">
              <a href="<?=BASEURL;?>Home/Peserta/<?=$sesi['nmSesi'];?>">
                <?php 
                list($hm,$jm) = explode(" ",$sesi['mulai']);
                list($hs,$js) = explode(" ",$sesi['selesai']);
                ?>
                Kls <?= $sesi['kelas'] ;?> Ruang Lab. <?= $sesi['nmLab'] ;?> <br/>
                <?= "{$hm} Jam {$jm} s.d {$js}" ;?>
              </a>
            </li>
            <?php endforeach; ?>
            <li class="list-group-item bg-dark">
              <a href="<?=BASEURL?>Home/results/1" class='text-light'>Nila Perolehan</a>
            </li>
        </div>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
