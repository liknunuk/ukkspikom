
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 bg-dark text-light text-center pg-head">
      <h4>UJI KOMPETENSI KOMPUTER SEKOLAH</h4>
      <h3>LKP PIKOM BANJARNEGARA</h3>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-md-12 text-center">
      <h4>PESERTA UJIAN</h4>
    </div>
  </div>    
  <div class="row">
    <div class="col-sm-12">
        <table class="table">
            <?php foreach($data['peserta'] AS $peserta):?>
                <tr>
                    <td>
                        <?=sprintf("%02d",$peserta['nmAbsen']);?>.&nbsp;
                        <?=$peserta['namaSiswa'];?> - [<?= $peserta['nilaiWP']; ?>,<?= $peserta['nilaiSS']; ?>]
                    </td>
                    <td width='60px'>
                        <a class="btn btn-primary" href="<?=BASEURL;?>Home/nilai/<?=$peserta['nmUrut'];?>">
                        Nilai
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <div class="text-center">
                <a href="<?=BASEURL;?>">Kembali</a>
        </div>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
